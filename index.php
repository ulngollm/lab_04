<?php
require_once 'php/classes/template.php';
require_once 'php/model.php';
require_once 'php/pagination.php';
require_once 'php/pagecontroller.php';
require_once 'php/authcontroller.php';
require_once 'php/menu.php';

$tpl = new Template('templates/index.tpl');
$tpl->SetValue('MENU', $menu);
$tpl->SetValue('TABLE', $pageTable);
$tpl->SetValue('AUTH', $auth_block);
$tpl->SetValue('PAGINATION', $pagination);

print($tpl->ToString());
