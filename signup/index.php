<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/classes/template.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/model.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/menu.php';

$tpl = new Template($_SERVER['DOCUMENT_ROOT'] . '/templates/index.tpl');
$tpl->SetValue('MENU', $menu);
$tpl->SetValue('AUTH', '');
if ($_GET && $_GET['result']) {
    $message = $_GET['result'] == 'OK'? 'Вы успешно зарегистрировались': 'Данный пользователь уже существует';
    $tpl->SetValue('TABLE',$message); 
} 
else {
    $tpl->SetValue('TABLE', file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/form.html'));
}
$tpl->SetValue('PAGINATION', '');
print($tpl->ToString());
