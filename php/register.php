<?php
require_once 'config.php';
require_once 'model.php';


if($_REQUEST['signup']){
    $name = htmlspecialchars($_REQUEST['name']);
    $mail = htmlspecialchars($_REQUEST['mail']);
    $password = md5(htmlspecialchars($_REQUEST['password']));

    addUser($name, $mail, $password, $db);
}

function isUserExists($email, $db){
    $result = $db->getOne("SELECT * from ?n where mail=?s",'user',$email);
    return $result;
}

function addUser($name, $mail, $password, $db){
    if(!isUserExists($mail,$db)){
        $db->query("INSERT INTO user(name, mail, password, date) VALUES(?s, ?s,?s, CURRENT_DATE())", $name,$mail,$password);
        header('Location: /signup/?result=OK');
    }
    else header('Location: /signup/?result=error');
}
