<?php

class Template
{

    public $values = array();
    public $html;

    public function __construct($tpl_name)
    {
        if(file_exists($tpl_name)){
            $this->html = file_get_contents($tpl_name);
        }
        else die("Не найден файл шаблона $tpl_name");
    }
    public static function FormatResult($sqlResult)
    {
        $result = "<table><thead>";
        $flagfirst = true;

        foreach($sqlResult as $row) {
            if ($flagfirst) {
                $key = array_keys($row);
                $result .= "<tr>";
                for ($i = 0; $i < count($key); $i++) {
                    $result .= "<th>$key[$i]</th>";
                }
                $flagfirst = false;
                $result .= "</tr></thead><tbody>";
            }
            $result .= '<tr>';
            foreach($row as $key => $value){
                $result .=  "<td>$value</td>"; 
            }
            $result .= "</tr>";
        }
        $result .= "</tbody></table>";
        return $result;
    }

    public function SetValue($key, $var)
    {
        $this->values["{{$key}}"] =  $var;
    }

    private function TemplateParse()
    {
        foreach($this->values as $entry => $replacer){
            $this->html = str_replace($entry, $replacer, $this->html);
        }
    }

    public function ToString(){
        $this->TemplateParse();
        return $this->html;
    }
}