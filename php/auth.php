<?php
require_once 'model.php';


if ($_REQUEST) {
    $mail = trim($_REQUEST['mail']);
    $password = trim($_REQUEST['password']);
    $authResult = authUser($mail, $password, $db);
    header("Location: /?$authResult");
}


function authUser($mail, $password, $db)
{
    $result = $db->getRow("SELECT mail, name, password from ?n where mail=?s", 'user', $mail);

    if (empty($result))
        return 'auth=NOT_EXIST';
    else {
        if ($result['password'] != md5($password))
            return 'auth=PASS_ERR';
        else {
            session_start();
            $_SESSION['name'] = $result['name'];
            $_SESSION['mail'] = $result['mail'];
            return 'auth=OK';
        }
    };
}
