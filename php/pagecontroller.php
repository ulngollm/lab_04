<?php
$page = isset($_GET['page']) ? $_GET['page'] : 1;
switch ($page) {
    case 1:
        $table = "Nomenclature";
        break;
    case 2:
        $table = "ContractingParties";
        break;
    case 3:
        $table = "SettlementsWithBuyers";
        break;
    case 4:
        $table = "NomenclatureRemains";
        break;
    case 5:
        $table = "NomenclatureSale";
        break;
}
$pageNum = isset($_GET['pagetab']) ? $_GET['pagetab'] : 1;
$result = $db->getAll("SELECT SQL_CALC_FOUND_ROWS * FROM ?n limit ?i OFFSET ?i", $table, PER_PAGE, ($pageNum - 1) * PER_PAGE);
$rowsCount = $db->getOne("SELECT FOUND_ROWS()");
$paginationController = new Pagination($rowsCount, $pageNum, $page);
$pagination = $paginationController->GetPagination();
$pageTable = Template::FormatResult($result);


