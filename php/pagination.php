<?php

class Pagination
{
    public $currentPage;
    public $maxPages = 6;
    public $pagesAround;
    public $section;
    public $pageCount;
    public $lowerPoint;
    public $upperPoint;



    public function __construct($rowsCount, $currentPage, $section)
    {
        $this->pageCount = $this->getPageCount($rowsCount);
        $this->currentPage = (int)$currentPage;
        $this->pagesAround = $this->maxPages / 2;
        $this->section = (int)$section;
        $this->lowerPoint = max(($currentPage - $this->pagesAround), 1);
        $this->upperPoint = min(($currentPage + $this->pagesAround), $this->pageCount);
    }

    public function getPageCount($rowsCount)
    {
        return ceil($rowsCount / PER_PAGE);
    }

    public function GetLink(int $index)
    {
        return ($index == $this->currentPage) ? "<span class=\"link link_active\">$index</span>" : "<a href=\"/?page=$this->section&pagetab=$index\" class=\"link\">$index</a>";
    }
    public function GetLinksRange()
    {
        $result = '';
        for ($i = $this->lowerPoint; $i <= $this->upperPoint; $i++) {
            $result .= $this->GetLink($i);
        }
        return $result;
    }
    public function GetPagination()
    {
        $pagination = ''; 
        if($this->pageCount > $this->maxPages + 1){
            if ($this->lowerPoint == 1) $this->upperPoint = $this->maxPages + 1;
            if ($this->upperPoint == $this->pageCount) $this->lowerPoint = $this->pageCount - $this->maxPages - 1;
        }
        if ($this->lowerPoint != 1) {
            $pagination .= $this->setFirstLinkPoint();
        }
        $pagination .= $this->GetLinksRange();
        if ($this->upperPoint != $this->pageCount) $pagination .= $this->setLastLinkPoint();
        return $pagination;
    }
    public function setFirstLinkPoint()
    {
        return "<a href=\"/?page=$this->section&pagetab=1\" class=\"link\">1</a><span>...</span>";
    }
    public function setLastLinkPoint()
    {
        return "<span>...</span><a href=\"/?page=$this->section&pagetab=$this->pageCount\" class=\"link\">$this->pageCount</a>";
    }
}
